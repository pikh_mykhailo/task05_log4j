package com.pikh;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Michail on 11/23/2019.
 */
public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        logger.trace("Trace message");
        logger.debug("Debug message");
        logger.info("Info message");
        logger.warn("Warn message");
        logger.error("Error message");
        logger.fatal("Fatal message");



    }
}
